//
//  AppDelegate.swift
//  RayTracing4
//
//  Created by Александр Баташев on 18.05.2018.
//  Copyright © 2018 Александр Баташев. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

