//
//  GLView.swift
//  RayTracing4
//
//  Created by Александр Баташев on 18.05.2018.
//  Copyright © 2018 Александр Баташев. All rights reserved.
//

import Cocoa
import OpenGL
import GLKit

class GLView: NSOpenGLView {
    
    let glv = View()

    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        CGLLockContext((self.openGLContext?.cglContextObj!)!)
        glv.draw()
        CGLFlushDrawable((self.openGLContext?.cglContextObj!)!)
        CGLUnlockContext((self.openGLContext?.cglContextObj!)!)
    
        // Drawing code here.
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let attr = [
            NSOpenGLPixelFormatAttribute(NSOpenGLPFAOpenGLProfile),
            NSOpenGLPixelFormatAttribute(NSOpenGLProfileVersion4_1Core),
            NSOpenGLPixelFormatAttribute(NSOpenGLPFAColorSize), 32,
            NSOpenGLPixelFormatAttribute(NSOpenGLPFAAlphaSize), 8,
            NSOpenGLPixelFormatAttribute(NSOpenGLPFADoubleBuffer),
            NSOpenGLPixelFormatAttribute(NSOpenGLPFADepthSize), 32,
            0
        ]
        
        self.wantsBestResolutionOpenGLSurface = true
        
        let format = NSOpenGLPixelFormat(attributes: attr)
        let context = NSOpenGLContext(format: format!, share: nil)
        
        self.openGLContext = context
        self.openGLContext?.makeCurrentContext()
        
        glv.initShaders()
    }
    
    func flush()
    {
        self.openGLContext?.flushBuffer()
    }
}
