//
//  View.swift
//  RayTracing4
//
//  Created by Александр Баташев on 18.05.2018.
//  Copyright © 2018 Александр Баташев. All rights reserved.
//

import Foundation
import GLKit

class View {
    
    var basicProgramId: UInt32 = 0
    var basicVertexShader: UInt32 = 0
    var basicFragmentShader: UInt32 = 0
    
    var vboHandlers: [GLuint] = [GLuint](repeating: 0, count: 1)
    
    func loadShaders(filename: String, shaderType: GLenum, progId: GLuint) -> UInt32 {
        let address = glCreateShader(shaderType)
        
       // todo load from file
        let name = filename.split(separator: ".")
        let shaderPath =  Bundle.main.path(forResource: String(name[0]), ofType: String(name[1]))
        
        if shaderPath == nil {
            print("path not found")
        } else {
            print(shaderPath!)
        }
        do {
            let shaderContent = try NSString(contentsOfFile: shaderPath!, encoding: String.Encoding.utf8.rawValue)
            let shaderStr = shaderContent.cString(using: String.Encoding.utf8.rawValue)
            var length = GLint(shaderContent.length)
            var glcSource = UnsafePointer<GLchar>? (shaderStr!)
            glShaderSource(address, 1, &glcSource, &length)
            glCompileShader(address)
            
            var compileStatus: GLint = GLint()
            glGetShaderiv(address, GLenum(GL_COMPILE_STATUS), &compileStatus)
            if (compileStatus == GL_FALSE) {
                print("Failed to compile shader")
                var value: GLint = 0
                glGetShaderiv(address, GLenum(GL_INFO_LOG_LENGTH), &value)
                var infoLog: [GLchar] = [GLchar](repeating: 0, count: Int(value))
                var infoLogLength: GLsizei = 0
                glGetShaderInfoLog(address, value, &infoLogLength, &infoLog)
                let str = NSString(bytes: infoLog, length: Int(infoLogLength), encoding: String.Encoding.ascii.rawValue)
                print(str ?? "error reading log")
            } else {
                glAttachShader(progId, address)
            }
            
        } catch {
            print("Unexpected error")
        }
    
        
        return address
    }
    
    func initShaders() {
        basicProgramId = glCreateProgram()
        
        basicVertexShader = loadShaders(filename: "basic.vert", shaderType: GLenum(GL_VERTEX_SHADER), progId: basicProgramId)
        basicFragmentShader = loadShaders(filename: "basic.frag", shaderType: GLenum(GL_FRAGMENT_SHADER), progId: basicProgramId)
        
        glLinkProgram(basicProgramId)
        
        let vertdata = [
            GLKVector3Make(-1, -1, 0),
            GLKVector3Make(1, -1, 0),
            GLKVector3Make(1, 1, 0),
            GLKVector3Make(-1, 1, 0)
        ]
        
        glGenBuffers(1, &vboHandlers)

        glBindBuffer(GLenum(GL_ARRAY_BUFFER), vboHandlers[0])
        glBufferData(GLenum(GL_ARRAY_BUFFER), vertdata.count*MemoryLayout<GLKVector3>.size, vertdata, GLenum(GL_STATIC_DRAW))
        print(MemoryLayout<GLKVector3>.size)

        var vaoHandles = [GLuint](repeating: 0, count: 1)

        glGenVertexArrays(1, &vaoHandles)
        glBindVertexArray(vaoHandles[0])

        glEnableVertexAttribArray(0)

        glVertexAttribPointer(0, 3, GLenum(GL_FLOAT), 0, 0, nil)
        
        
        
//        let uniform_aspect = glGetUniformLocation(basicProgramId, "aspect")
//
//        glUniform1f(uniform_aspect, 1)
        
        
        var status: GLint = 0
        glGetProgramiv(basicProgramId, GLenum(GL_LINK_STATUS), &status)
        if status != 0 {
            var value: GLint = 0
            glGetProgramiv(basicProgramId, GLenum(GL_INFO_LOG_LENGTH), &value)
//            glGetShaderiv(address, GLenum(GL_INFO_LOG_LENGTH), &value)
            var infoLog: [GLchar] = [GLchar](repeating: 0, count: Int(value))
            var infoLogLength: GLsizei = 0
//            glGetShaderInfoLog(address, value, &infoLogLength, &infoLog)
            glGetProgramInfoLog(basicProgramId, value, &infoLogLength, &infoLog)
            let str = NSString(bytes: infoLog, length: Int(infoLogLength), encoding: String.Encoding.ascii.rawValue)
            print(str ?? "error reading log")
        }
        print(status)
    }
    
    func draw() {
        
        glUseProgram(basicProgramId)
        glBindBuffer(GLenum(GL_ARRAY_BUFFER), vboHandlers[0])
        glDrawArrays(GLenum(GL_TRIANGLE_FAN), 0, 4)
        glUseProgram(0)
//        glDrawArrays(GLenum(GL_TRIANGLES), 0, 3)
        
    }
}
