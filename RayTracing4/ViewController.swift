//
//  ViewController.swift
//  RayTracing4
//
//  Created by Александр Баташев on 18.05.2018.
//  Copyright © 2018 Александр Баташев. All rights reserved.
//

import Cocoa
import OpenGL
import GLKit

class ViewController: NSViewController {

    @IBOutlet weak var glView: NSOpenGLView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        glView.prepareOpenGL()
        
        

        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

